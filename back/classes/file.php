<?php
  class File{
    private $path;
    private $name;
    private $filename;
    private $tmpName;
    private $extension;
    private $size;

    public function __construct(array $donnees){
        if (!empty($donnees)){
            $this->hydrate($donnees);
        }    
    }
  
    //automatically hydrate class from an array
    public function hydrate(array $donnees){
      foreach ($donnees as $key => $value){
        $method = 'set'.ucfirst($key);
                  
        if (method_exists($this, $method)){
          $this->$method($value);
        }
      }
    } 

    public function toObject(){
      $data = (object)[];
      $data->name = $this->name;
      $data->filename = $this->filename;
      $data->extension = $this->extension;
      $data->size = $this->size;

      return $data;
    }
    

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of filename
     */ 
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of filename
     *
     * @return  self
     */ 
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get the value of tmpName
     */ 
    public function getTmpName()
    {
        return $this->tmpName;
    }

    /**
     * Set the value of tmpName
     *
     * @return  self
     */ 
    public function setTmpName($tmpName)
    {
        $this->tmpName = $tmpName;

        return $this;
    }

    /**
     * Get the value of extension
     */ 
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set the value of extension
     *
     * @return  self
     */ 
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get the value of size
     */ 
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */ 
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get the value of path
     */ 
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     *
     * @return  self
     */ 
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
  }
?>