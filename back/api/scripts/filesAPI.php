<?php
  require_once __DIR__.'/../../database/database.php';
  require_once __DIR__.'/../../utils/utils.php';
  require_once __DIR__.'/../../classes/file.php';
  
  class FilesAPI{
    public static function upload($package, $restFiles){
      $i = 0;
      $restFiles = (Array)$restFiles;
      $files = [];
      // retrieve every files and add them in an array
      while(isset($restFiles['file_'.$i])){
        $file = $restFiles['file_'.$i];
        $additional = pathinfo($file['name']);

        $donnees = (object)[];
        $donnees->name = $file['name'];
        $donnees->filename = $additional['filename'];
        $donnees->extension = $additional['extension'];
        $donnees->tmpName = $file['tmp_name'];
        $donnees->size = $file['size'];
        $donnees->path = $package->getPath();

        $file = new File((Array)$donnees);

        $files[] = $file;
        $i++;
      }
      // if no files found, generate error
      if($i == 0){
        Utils::error(400, 'No files founds.');
      }

      //save files in database
      Database::files()::save($files);

      return $files;
    }
  } 
?>