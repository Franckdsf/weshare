<?php
require_once __DIR__.'/../database.php';
require_once __DIR__.'/../../classes/package.php';

class PackagesDB{

  // converts a package from DB into an object of package class
  private static function packageDBtoPackage($packageDB){
    $packageDB['path'] = $packageDB['id'];
    $package = new Package($packageDB);

    if(!file_exists(dirname(dirname(__DIR__)).'/database/storage/files/'.$package->getPath().'/'.$package->getName()))
      Utils::error(500, 'filesDB : error file does not exist : '.$package->getPath().'/'.$package->getName());

    return $package;
  }

  //finds a package by id. Returns null if nothing found
  public static function findById($id){
    self::cleanDatabase();

    Database::instance()->beginTransaction();
    try{
      $query = Database::instance()->prepare("SELECT * FROM packages WHERE id = :id");
      $query->bindValue(':id', $id);
      $query->execute();
      Database::instance()->commit();
    
      $package = $query->fetch();
  
      if(!$package)
        return null;
      else{
        $package = self::packageDBtoPackage($package);
        return $package;
      }  
    }
    catch(Exception $error){
      Database::instance()->rollback();
      Utils::error(500, 'internal error while trying to find a package by id : '.$error->getMessage());
    }
  }
  
  //finds a package by id. Throws error if error catched
  public static function saveOne($package){
    //generate a unique random id not in the database yet
    $id = null;
    do{
      $id = Database::generateId();
      if(self::findById($id) != null)
        $id = null;
    } while($id == null);

    $package->setId($id);
    $package->setPath($id);
    $package->setName("we_share_".$package->getId().".zip");

    Database::instance()->beginTransaction();
    try{
      $query = Database::instance()->prepare("INSERT INTO packages (id, name, message) VALUES (:id, :name, :message)");
      $query->bindValue(':id', $package->getId());
      $query->bindValue(':name', $package->getName());
      $query->bindValue(':message', $package->getMessage());
      $query->execute();
      Database::instance()->commit();
    
      return $package;
    }
    catch(Exception $error){
      Database::instance()->rollback();
      Utils::error(500, 'internal error while creating a package : '.$error->getMessage());
    }
  }

  public static function cleanDatabase(){
    Database::instance()->beginTransaction();
    try{
      $query = Database::instance()->prepare("SELECT * from packages where DATE(created_at) < DATE('now','-7 days')");
      $query->execute();
    
      //delete all files & sub files from the server
      function deleteAll($dir) {
        foreach(glob($dir . '/*') as $file) {
            if(is_dir($file))
                deleteAll($file);
            else
                unlink($file);
        }
        rmdir($dir);
      }

      foreach($query->fetchAll() as $package){
        //delete all files from the package
        $request = Database::instance()->prepare("DELETE FROM files where package = :package");
        $request->bindValue(':package', $package['id']);
        $request->execute();

        //delete the package itself
        $request = Database::instance()->prepare("DELETE FROM packages where id = :id");
        $request->bindValue(':id', $package['id']);
        $request->execute();

        //if package exist, delete if from server
        if(file_exists(dirname(dirname(__DIR__)).'/database/storage/files/'.$package['id']))
          deleteAll(__DIR__.'/../storage/files/'.$package['id']);
      }

      Database::instance()->commit();
    }
    catch(Exception $error){
      Database::instance()->rollback();
      Utils::error(500, 'internal error while cleaning database : '.$error->getMessage());
    }

  }
}

?>