import {$} from '../../assets/js/jquery_like.js';

class DownloadFilesComponent{
  files = [];
  baseURL = window.origin+'/api';

  constructor(){
    //if user isn't trying to retrieve files, return
    if(window.location.pathname == "/")
      return
    
    const self = this;

    //get files based on URL id
    fetch(this.baseURL+'/files'+window.location.pathname).then(async result => {
      if(result.ok)
        return result.json();
      throw await result.json();
    })
    // if files successfully retrieved, display them on screen
    // and set trigger to button to download .zip
    .then(async result => {
      result = result.data;
      result.files.forEach(file => {
        this.previewFile(file);
      });
      this.setMessage(result.message);
      this.setDownloadButton(self, result);
      $("#download_icon .fa-arrow-circle-down").style.display = "block";
    }).catch(() => {
      //if error, display error on screen
      $("#download_icon .fa-exclamation-circle").style.display = "block";
      $(".download_files .message_cont").remove();
      $(".download_files #button_link").innerHTML = "No files found";
    });
  }

  //show file in view
  previewFile = (file) => {
    // let size = Math.floor(file.size / 1000);
    const div = document.createElement("div");
      div.innerHTML = "<div class='name'>"+file.name+"</div>";
      // div.innerHTML += "<div class='size'>"+size+" Ko</div>";
  
    $(".download_files > #files_cont").append(div);
  }

  setMessage = (message) => {
    $(".download_files .message").innerHTML = message;
  }

  //download button function to download .zip
  setDownloadButton = (self, zip) => {
    $(".download_files #button_link");
    $(".download_files #button_link").classList.remove("inactive");
    $(".download_files #button_link").addEventListener("click", () => {
      fetch(self.baseURL+'/download/'+zip.id).then(async res => {
        const blob = await res.blob();
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = zip.name;
        document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
        a.click();    
        a.remove();  //afterwards we remove the element again        
      });
    });
  }
}

window.addEventListener("load", () => {
  new DownloadFilesComponent();
});