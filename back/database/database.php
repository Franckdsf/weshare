<?php
  require_once __DIR__.'/scripts/filesDB.php';
  require_once __DIR__.'/scripts/packagesDB.php';

  class Database{
    static $db = null;
    static $files = null;
    static $packages = null;

    public static function init(){
      if(self::$db != null)
        return;

      try{
        
        self::$db = new PDO('sqlite:'.__DIR__.'/../database/storage/database.sqlite');
        self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
        self::$files = new FilesDB();
        self::$packages = new PackagesDB();

      } catch(Exception $e) {
          Utils::error(500, "Can't create SQLite database: ".$e->getMessage());
          die();
      }
    }

    public static function run($script){
      self::$db->exec($script);
    }

    public static function instance(){
      return self::$db;
    }

    public static function generateID($length = 10){
      return substr(str_shuffle(str_repeat($x='123456789ABCDEFGHIJKLMNPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);  
    }

    public static function files(){
      return self::$files;
    }

    public static function packages(){
      return self::$packages;
    }
  }

  Database::init();
?>