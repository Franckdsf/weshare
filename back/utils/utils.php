<?php
  class Utils{
    
    // generate an JSON Error and die
    static function error($number, $message){
      $error = (object)[];
      $error->error = $message;
      echo JSON_encode($error);
      
      header('Content-Type: application/json');
      http_response_code($number);
      die;
    }

    // generate a JSON Response and die
    static function success($number, $message){
      $success = (object)[];
      $success->data = $message;
      echo JSON_encode($success);

      header('Content-Type: application/json');
      http_response_code($number);
      die;
    }

    //delete all files from the server
    private static function deleteAllFiles($dir){
      foreach(glob($dir . '/*') as $file) {
        if(is_dir($file))
          self::deleteAllFiles($file);
        else
          unlink($file);
      }
      rmdir($dir);
    }
    
    // install database
    static function installDB(){
      require_once __DIR__.'/../database/database.php';
      try{
        //reset database
        $file = file_get_contents(__DIR__.'/bdd.sql');
        Database::run($file);

        //if files folder exist, delete it and recreate an empty one
        if(file_exists(dirname(__DIR__).'/database/storage/files'))
          self::deleteAllFiles(__DIR__.'/../database/storage/files');
        mkdir(__DIR__."/../database/storage/files");

        return true;
      }
      catch(Exception $er){
        throw new Exception($er);
      }
    }
  }
?>