

# 						WESHARE

Le site Internet permettant le partage simple et rapide de fichiers. 

**we.share** a été développé par **Franck DESFRANCAIS** et **Loïs CHABRIER** dans le cadre du module Ingénierie du Web. Le back-end tire parti du **PHP** et **SQLite** tandis que le front-end profite du **JS, SCSS, HTML, CSS (et PHP).** Le tout a évidemment été développé en utilisant **Git** et **Visual Studio Code**.

### Sommaire

- WESHARE - DESCRIPTION
  - Le site
  - Les fonctionnalités
  - Les évolutions
- WESHARE - ARCHITECTURE DU PROJET
  - Le back
  - Le front
  - Informations complémentaires





# WESHARE - Description



## 1. Le site

Le site a pour vocation de devenir une plateforme de transfert de fichiers rapides, simple, et efficace. Pour cela nous avons opté pour un design simple et épuré permettant à l'utilisateur de téléverser ses fichiers d'un simple clique. 

Grâce à un glisser / déposer (ou via un clic sur *Add a file*), le site va automatiquement générer un lien unique a votre téléversement que vous pourrez envoyer à vos amis afin qu'ils puissent télécharger vos fichiers.

![Capture](images\accueilsite.png)



Une fois le lien reçu, il suffira de le coller dans un navigateur Internet quel qu'il soit, et cliquer sur *Download files* pour télécharger les fichiers dans un format *.zip*



![Capture](images\retrieve.png)



Simple, rapide et efficace, **WeShare** est LA solution de facilité pour le partage de fichiers.

Pour installer l'application web sur votre serveur, suivez les instructions du **README.md** à la racine du .zip 



## 2. Les fonctionnalités

### 2.1. Le fond d'écran

Le fond d'écran du site change automatiquement toutes les **X** secondes en allant récupérer aléatoirement une image sur l'API de *picsum.photos*. Si une image n'est pas présente sur leur serveur, une autre requête est effectuée. 

A noter qu'afin d'améliorer l'expérience sur le site, le background ne change qu'une fois que l'image est entièrement chargée.


### 2.1. L'upload de fichiers

Ayant pour but d'être une plateforme très simple d'utilisation nous avons fait le choix de nous attarder sur l'expérience utilisateur lors de l'import de fichiers. Ainsi, nous avons développé 2 solutions de téléversement :

Utiliser le bouton ***Add a file*** qui ouvrira un explorateur de fichiers sur lequel aller chercher le ou les documents à téléverser.

<img src="images\uplaod.png" style="float:left" alt="">

Utiliser le ***drag & drop*** (glisser  / déposer) pour téléverser vos documents. A noter que vous pouvez directement téléverser vos fichiers depuis la page de téléchargement de fichiers grâce au drag & drop. Cela vous redirigera automatiquement vers la page d'envoi de fichiers.

<img src="images\draganddrop.png" style="float:left" alt="">



Egalement, vous avez la possibilité d'inclure un message personnalisé avec les fichiers.


 ![Capture](images\uploadmessage.png)





### 2.2. Les sécurités

Certaines sécurités ont été mises en place de sorte que la base de données ne soit pas surchargée par quelqu'un de mal intentionné, qu'une personne ne puisse pas injecter du code SQL dans celle ci et qu'elle ne puissent pas exécuter un script depuis des fichiers téléversés.

Le volume total de téléversement ne doit pas dépasser 20MB. 

![Capture](images\websitesecurity.png)

De plus :

- Il n'est pas possible de téléverser plus de 500 fichiers en même temps.
- Les injections SQL ont étés prévenues grâce au PDO.
- De même que l'exécution de scripts par le téléversement de fichiers, puisque nous les stockons sous format *.zip*.
- Nous avons également bloqué les requêtes distantes et n'avons autorisé que les requêtes provenant du serveur même.



De plus nous avons mis en place une **gestion d'erreurs accrues** que ce soit dans le front ou dans le back afin de prévenir les failles de sécurités et les mauvaises manipulations d'un utilisateur.



### 2.3. Suppression automatique de fichiers

Pour des raisons d'encombrement de la base de données, les fichiers stockés et les champs associés sont automatiquement détruits après une durée de *7 jours*.



### 2.4. Réinitialisation de la base de données

Même s'il n'y a pas besoin d'installer la base de données au départ, il est néanmoins possible de l'installer / réinitialiser manuellement. En revanche, nous avons protégé par mot de passe l'installation de façon que seules les personnes aptes puissent le faire.

Avec le lien suivant, l'API demande un mot de passe pour la réinstallation : *http://we.share/api/database/reinstall*

```json
{
    "error":"password required in query field"
}
```

Avec celui-ci, l'API retourne une erreur suite à un mot de passe erroné : http://we.share/api/database/reinstall?password

```json
{
    "error":"can't install database, wrong password"
}
```

Enfin, en fournissant le mot de passe valide, l'installation s'effectue avec succès (le mot de passe est ***somePassword***) : *http://we.share/api/database/reinstall?password=somePassword*

```json
{
    "data":"Database successfully init \/ reset"
}
```



## 3. Les évolutions

### 3.1 Ouvrir l'API

Comme nous avons développé une **API RESTFUL**, nous pouvons tout à fait ouvrir des points d'accès à des projets tiers afin qu'ils tirent parti de notre application. Nous voulons ainsi permettre à d'autres applications de stocker leurs fichiers sur notre serveur.

### 3.2 Améliorer le téléversement

Aujourd'hui, nous ne pouvons que téléverser des fichiers, mais il est prévu de pouvoir téléverser des dossiers et leurs contenus (fichiers, sous-dossiers, sous-fichiers, etc.)

### 3.3 Envoi de fichiers par mail

Il serait également intéressant de pouvoir envoyer le lien pour télécharger les fichiers directement par mail.





# WE SHARE - Architecture du projet

Nous avons voulu créer un projet facilement maintenable, avec capacité de scaling importante afin de pouvoir créer un projet plus ambitieux à partir de celui-ci.

Nous avons organisé le code selon l'architecture suivante :

```
📦weshare
 ┣ 📂back
 ┣ 📂front
 ┣ 📜.htaccess
```

Le **back** comporte tous les scripts permettant de créer une **API RESTFUL** connectée à une base de données SQLite.
Le **front** est lui composé de tous éléments nécessaires pour afficher les pages à l'écran et faire les appels à **l'API** (le back).
Le **.htaccess** permet de faire la redirection vers le back ( /api/ ) ou le front ( / ) selon l'URL.



## 1. Le back

Le back est décomposé ainsi :

```js
📦weshare
 ┣ 📂back
 ┃ ┃ // tous les scripts permettant de faire la liaison entre
 ┃ ┃ // les appels Client et la base de données 
 ┃ ┣ 📂api
 ┃ ┃ ┣ 📂scripts
 ┃ ┃ ┃ ┣ 📜filesAPI.php
 ┃ ┃ ┃ ┗ 📜packageAPI.php
 ┃ ┃ ┃ // Le script qui redirige vers filesAPI ou packageAPI
 ┃ ┃ ┗ 📜api.php
 ┃ ┃ // les classes des tables de la base de données
 ┃ ┣ 📂classes
 ┃ ┃ ┣ 📜file.php
 ┃ ┃ ┗ 📜package.php
 ┃ ┃ // tout ce qui concerne la base de données
 ┃ ┣ 📂database
 ┃ ┃ ┃ // les scripts permettant de faire la liaison entre le serveur et SQLite
 ┃ ┃ ┣ 📂scripts
 ┃ ┃ ┃ ┣ 📜filesDB.php
 ┃ ┃ ┃ ┗ 📜packagesDB.php
 ┃ ┃ ┃ // ici est contenu les fichiers de base de données :
 ┃ ┃ ┣ 📂storage
 ┃ ┃ ┃ ┃ // files => tous les fichiers stockés (avec ID unique)
 ┃ ┃ ┃ ┣ 📂files
 ┃ ┃ ┃ ┃ ┗ 📂RFT1VJXNG6
 ┃ ┃ ┃ ┃ ┃ ┗ 📜we_share_RFT1VJXNG6.zip
 ┃ ┃ ┃ ┃ // database.sqlite => le fichier SQL de base de données
 ┃ ┃ ┃ ┗ 📜database.sqlite
 ┃ ┃ ┃ // Le script qui redirige vers filesDB ou packagesDB + permet d'executer des scripts SQL
 ┃ ┃ ┗ 📜database.php
 ┃ ┃ // les fichiers utilitaires
 ┃ ┣ 📂utils
 ┃ ┃ ┃ // le script d'installation de la base de données
 ┃ ┃ ┣ 📜bdd.sql
 ┃ ┃ ┃ // le script du routeur permettant de rediriger sur les bonnes routes, récupérer les paramètres ect..
 ┃ ┃ ┃ // (100% développé par Franck)
 ┃ ┃ ┣ 📜router.php
 ┃ ┃ ┃ // les scripts utilitaires : installation de Database, Générateur d'Erreur JSON, ...
 ┃ ┃ ┗ 📜utils.php
 ┃ ┃ // le routeur (permettant de gérer les entrées)
 ┗ ┗ 📜index.php
```



## 2. Front

L'architecture du front a pour but de ressembler à ce qu'il se fait aujourd'hui avec les frameworks JS tels que ***vue*** ou ***react***. Le but étant de diviser le projet en pages (Home, About, Donate) et composants (éléments qui composent une page). Les composants ont pour vocation d'être modulaires et réutilisables tandis que les pages servent de conteneurs à ceux-ci.

Nous avons ici une architecture typique d'un projet **VueJS**.
Nous avons également fait le choix de se séparer* du PHP afin d'avoir un rendu **Client Side** plutot que **Server Side**.

```js
📦weshare
 ┣ 📂front
 ┃ ┃ // fichiers images, scripts, ect..
 ┃ ┣ 📂assets
 ┃ ┃ ┣ 📂img
 ┃ ┃ ┃ ┣ 📜logo.png
 ┃ ┃ ┃ ┗ 📜og.jpg
 ┃ ┃ ┗ 📂js
 ┃ ┃ ┃ ┣ 📜fade.js
 ┃ ┃ ┃ ┗ 📜jquery_like.js
 ┃ ┃ // composants et leurs fichiers (éléments de composition)
 ┃ ┣ 📂components
 ┃ ┃ ┣ 📂background
 ┃ ┃ ┃ ┣ 📜background.css
 ┃ ┃ ┃ ┣ 📜background.js
 ┃ ┃ ┃ ┣ 📜background.php
 ┃ ┃ ┃ ┗ 📜background.scss
 ┃ ┃ ┣ 📂downloadFiles
 ┃ ┃ ┃ ┣ 📜downloadFiles.css
 ┃ ┃ ┃ ┣ 📜downloadFiles.js
 ┃ ┃ ┃ ┣ 📜downloadFiles.php
 ┃ ┃ ┃ ┗ 📜downloadFiles.scss
 ┃ ┃ ┗ 📂uploadFiles
 ┃ ┃ ┃ ┣ 📜uploadFiles.css
 ┃ ┃ ┃ ┣ 📜uploadFiles.js
 ┃ ┃ ┃ ┣ 📜uploadFiles.php
 ┃ ┃ ┃ ┗ 📜uploadFiles.scss
 ┃ ┃ // les différentes vues (il n'y en a qu'un aujourd'hui, mais on peut imaginez qu'il y ai : About, Donate, .. )
 ┃ ┣ 📂views
 ┃ ┃ ┗ 📂home
 ┃ ┃ ┃ ┣ 📜home.css
 ┃ ┃ ┃ ┣ 📜home.js
 ┃ ┃ ┃ ┣ 📜home.php
 ┃ ┃ ┃ ┗ 📜home.scss
 ┃ ┃ // le fichier principale qui permet de charger les vues (aujourd'hui il n'y a que la page home)
 ┃ ┗ 📜app.php
```



## 3. Informations complémentaires

### 3.1. Back - Routeur

Le but a été de créer un routeur facile d'utilisation, fonctionnel et complet. Il est semblable à un routeur **ExpressJS** de part sa syntaxe. Exemple : Nous voulons créer un lien **get** vers **/api/files/AJGIOES** où AJGIOES est l'id d'un zip à récupérer.

```typescript
// permet de rajouter un string au début de toutes les routes qui suivent
Router::baseURL('/api');

# Ex : si l'utilisateur tape bien sur /api/files/AJGIOES avec la méthode GET, il rentre dans la fonction
// $route est une variable contenant toutes les informations d'une route
// $route->path : l'URL de la requête
// $route->params : Objet avec pour variables les clé définies dans l'URL et les valeurs passées dans l'URL
// $route->query : Object avec pour variables les queries de l'URL ($_GET)
// $route->body : Object avec toutes les variables ($_POST)
// $route->files : Object avec tous fichiers ($_FILES)
Router::get('/files/:id', function($route){
    echo $route->params->id; # output : AJGIOES
    die;
}

// Autre exemple

Router::baseURL('/secret');
# Ex : si l'utilisateur tape sur /secret/messages/franck/2020 avec la méthode POST, il rentre dans la fonction
Router::post('/messages/:user/:date', function($route){
    echo $route->params->user; # output : franck
    echo $route->params->date; # output : 2020
    die;
}
# Ex : si l'utilisateur tape sur /secret/messages/franck avec la méthode GET, il rentre dans la fonction
Router::get('/messages/:user', function($route){
    echo $route->params->user; # output : franck
    die;
}
```



### 3.2. Back - Réponses

Deux fonctions sont conçues pour envoyer un résultat au client et ainsi assurer l'intégrité des réponses:

```php
 Utils::error(500, 'internal error while cleaning database : '.$error->getMessage());
```

```json
# Response code 500
{
	"error": "internal error whil cleaning database : Access denied.",
}
```

```php
Utils::success(201, $user->toObject());
```

```json
# Response code 201
{    
    "data": {
        "firstName": "Franck",
        "lastName": "Desfrançais"
    }
}
```

Elles mettent fin à l'exécution de scripts dans le back et assurent de renvoyer une réponse en JSON, formatée, et avec un code réponse.

### 3.4. Back - Base de données

<img src="images\db_diagram.png" style="float:left" alt="" width="300">



### 3.5. Front - CSS & SCSS

Les fichiers **css** sont compilés à partir du **scss** et chaque composant / page possède son CSS. cela permet d'éviter de charger trop de CSS en une fois.



### 3.6. Front - Import de composants

Il n'est aujourd'hui pas possible d'importer des fichiers HTML dans un autre fichier HTML. C'est pourquoi nous avons eu recours au PHP dans le front afin d'effectuer les imports de composants.

```php+HTML
<div class="container" id="background_parent">
    <div class="background_component"><?php require __DIR__.'/../../components/background/background.php' ?></div>
</div>
```
