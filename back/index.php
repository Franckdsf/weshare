<?php
  require_once __DIR__.'/utils/router.php';
  require_once __DIR__.'/utils/utils.php';
  require_once __DIR__.'/api/api.php';

  //if database not already installed
  if(!is_dir(__DIR__.'/database/storage/files')){
    //install it
    try{
      Utils::installDB();
    }
    //throw error if something occured
    catch(Exception $er){
      Utils::error(500, "Can't install database: ".$er->getMessage());
    }
  }
  
  Router::baseURL('/api');

  Router::delete('/files/:id', function($route){

    Utils::error(500, 'Trying to delete item : '.$route->params->id.'. Function not implemented yet');

  });

  Router::post('/files', function($route){

    Api::packages()::save($route->files, $route->body->message);

  });

  Router::get('/files/:id', function($route){
    
    Api::packages()::findById($route->params->id);
    
  });

  Router::get('/download/:id', function($route){

    //check if file exist on server, if not, throw error
    if(!file_exists(__DIR__.'/database/storage/files/'.$route->params->id.'/we_share_'.$route->params->id.'.zip'))
      Utils::error(500, 'filesDB : error file does not exist : '.$route->params->id);

    //redirect to file path to download it
    header('Location: /back/database/storage/files/'.$route->params->id.'/we_share_'.$route->params->id.'.zip');
    die;
  });

  Router::get('/database/reinstall', function($route) {

    //if no password entered in URL
    if(!isset($route->query->password))
      Utils::Error('400', 'password required in query field');

    //if wrong password
    if($route->query->password != 'somePassword')
      Utils::Error('400', "can't install database, wrong password");
  
    //install database
    try{
      Utils::installDB();
      Utils::success(200, 'Database successfully installed.');
    }
    //return error if something occurs
    catch(Exception $er){
      Utils::error(500, "Can't install database: ".$er->getMessage());
    }
  });

  Utils::Error('400', 'Bad request, no route found.');
?>