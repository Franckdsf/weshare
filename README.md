# WE SHARE - INSTALLATION



## 1. Création du répertoire racine 

Avant toutes choses, il faut créer un répertoire à la racine du serveur web, portant le nom du site (par convention et donc non obligatoire) :

```bash
sudo mkdir /var/www/we.share
```



## 2. Créer un virtual host

```bash
sudo nano /etc/apache2/sites-available/we.share.conf  
```

```xml
<VirtualHost *>
    DocumentRoot "/var/www/we.share"
    ServerName we.share

    <Directory "/var/www/we.share">
        Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
    </Directory>
</VirtualHost>
```



## 3. Ajout de l'enregistrement DNS

Enfin, ne pas oublier de rajouter la ligne suivante dans le fichier ***/etc/hosts*** pour la résolution de nom : 

```xml
127.0.0.1       we.share
```



## 4. Importez les fichiers

La dernière étape consiste simplement à extraire le paquet dans la racine du projet.



## 5. Profitez

Pour accéder au site, rien de plus simple, il suffit de lancer avec n'importe quel navigateur *http://we.share/*.

Nous avons fait en sorte que la base de données soit installée par défaut, cela permet un déploiement en un clic *(il suffit d'extraire le .zip dans la racine du serveur)*.

