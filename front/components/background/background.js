import {fadeIn, fadeOut} from '../../assets/js/fade.js';
import {$} from '../../assets/js/jquery_like.js';

class BackgroundComponent{

  constructor(){
    this.ImageBackgroundLoop();
  }

  ImageBackgroundLoop() {
    // get a random image url from API
    const number = Math.floor(Math.random() * 200);
    const bgImg = new Image();
    const url = "https://picsum.photos/id/"+number+"/2300/1500";
    const self = this;

    // get the first element
    let previous_background = $("#background_component").firstElementChild;
    // if it's not a background div, create it
    if(!previous_background || !previous_background.classList.contains("background")){
      previous_background = document.createElement("div");
      previous_background.classList.add("background");
      previous_background.style.backgroundImage = "url('"+url+"')"; 
      $("#background_component").prepend(previous_background);
    }
  
    // if image doesnt exist on API
    // retry with an other image
    bgImg.onerror = function(){
      self.ImageBackgroundLoop();
    };
    // if image successfully loaded
    bgImg.onload = function(){
      // create background div
      let background = document.createElement("div");
      background.classList.add("background");
      background.style.backgroundImage = "url('"+url+"')"; 
      $("#background_component").prepend(background);
  
      // fade out the previous background
      setTimeout(() => {
        fadeOut(previous_background);            
      }, 1000);
  
      // remove the previous background div
      // repeat this function
      setTimeout(() => {
        previous_background.remove();
        self.ImageBackgroundLoop();      
      }, 7000);  
    };
    bgImg.src = url;
  }
}

window.addEventListener("load", () => {
  new BackgroundComponent();
})